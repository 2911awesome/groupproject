package groupAssignment;

import java.util.Random;
/**
 * this class is simply to put up walls in an proability distrubution around the solution, so literally a wall producer 
 * it will setup a grid to be used, then take in the solution from puzzle solution and place walls in relation to the solution
 * @author Dan
 *
 */

public class PuzzleWall {

	
	
	

	/**
	 *  this function will put walls up in differring probability away from the solution, and the probabilty will difference on distance
	 *  away from the solution, distance unit away from solution will have a probabilty of 30%, 2 units away is 60% and 3units away is a confirm wall
	 * @param solution
	 * @return
	 */
	public Map SetWalls(Map solution,int diffculty){
		
		Random randGen =new Random();
		int distance = diffculty;
		int y = 0;
		int x = 0;
		
		//just to ensure code does not break and walls dun need to be so far away
		if(distance > 3){
			distance = 3;
		}
		
		while(y <= 12){
			while(x <= 12){
				int randomInt = randGen.nextInt(100);
				if(isNotWallOrEmpty(x,y, solution)){
					
					switch(distance){
					
					case 1: distance = 1;
						
						if(randomInt < 30){
							distance = 1;
							checkChangeTile(x,y,solution, distance);
						}
						break;
					case 2: distance = 2; 
						if(randomInt < 60){
							distance = 2;
							checkChangeTile(x,y,solution,distance);
						}
					break;
					case 3: distance = 3;
					
						if(randomInt<101){
							distance = 2; 
							checkChangeTile(x,y,solution,distance);
						}
						break;
					}
					
					
				}
				x++;
			}
			y++;
		}
		return solution; 
	}
	
	/**
	 * this will just check is the tile is wall or empty space 
	 * @param x
	 * @param y
	 * @return
	 */
	
	public boolean isNotWallOrEmpty(int x, int y, Map Map){
		
		if(Map.getTileType(x,y) != TileType.MAN){
			return true;
		}if(Map.getTileType(x,y) != TileType.SOLUTION){
			return true;
		}if(Map.getTileType(x,y) != TileType.BOX){
			return true;
		}if(Map.getTileType(x,y) != TileType.GOAL){
			return true;
		}if(Map.getTileType(x,y) != TileType.GOALBOX){
			return true;
		}else{
			return false;
		}
		
		
	
		/**else if(grid[x][y].isEmpty() || grid[x][y].isWall() ){
			return false; 
		}**/
	}
	
	/**
	 * this will take in a solution's position on the board, then it will check if everything surrounding that one position is a empty space. if it is, then it will change
	 * it to a wall tile type
	 * @param x
	 * @param y
	 */
	
	public void checkChangeTile(int x, int y, Map solution,int distance){
		
		int i = 0; //counter for while loop
		int tempX = x - distance; 
		int tempY = y - distance;
		
		while(i <= distance){
			while(tempY <= y ){
				while(tempX <= x){
					if(isNotWallOrEmpty(tempX,tempY,solution)){
						solution.buildWall(tempX, tempY);
					}
					
					tempX++;
				}
				tempY++;
			}
			i++;
		}
		/**if(isNotWallOrEmpty(x,y+distance)){
			solution.buildWall(x,y+distance);					HARD CODED OUT THE WALL MAKING AROUND THE SOLUTION TILE
		}
		if(isNotWallOrEmpty(x,y-distance)){
			solution.buildWall(x, y-distance);
		}
		if(isNotWallOrEmpty(x-distance,y)){
			solution.buildWall(x-distance, y);
		}
		if(isNotWallOrEmpty(x-distance,y+distance)){
			solution.buildWall(x-distance, y+distance);
		}
		if(isNotWallOrEmpty(x-distance,y-distance)){
			solution.buildWall(x-distance, y-distance);
		}
		if(isNotWallOrEmpty(x+distance,y)){
			solution.buildWall(x+distance, y);
		}
		if(isNotWallOrEmpty(x+distance,y+distance)){
			solution.buildWall(x+distance, y+distance);
		}
		if(isNotWallOrEmpty(x+distance,y-distance)){
			solution.buildWall(x+distance, y-distance);
		}**/
	}
	public Map removeWall(Map input, int diffculty){
		
		int x = 0;
		int y = 0;
		int i = diffculty;
		int tempX = x - diffculty;
		int tempY = y - diffculty;
		Random randGen =new Random();
		
		
		while (y < 12 ) {
			while(x < 12){
				if(input.getTileType(x, y) == TileType.BOX || 
				input.getTileType(x, y) == TileType.GOAL ||
				input.getTileType(x, y) == TileType.MAN){
					while(i <= diffculty){
						while(tempY <= y ){
							while(tempX <= x){
								int randomInt = randGen.nextInt(100);
								if(input.getTileType(tempX, tempY) == TileType.WALL){
									if(randomInt < 60){
										input.setEmpty(tempX, tempY);
									}
								}
								
								tempX++;
							}
							tempY++;
						}
						i++;
					}
					while(i <= diffculty){
						while(tempY >= y ){
							while(tempX >= x){
								int randomInt = randGen.nextInt(100);
								if(input.getTileType(tempX, tempY) == TileType.WALL){
									if(randomInt < 60){
										input.setEmpty(tempX, tempY);
									}
								}
								
								tempX--;
							}
							tempY--;
						}
						i++;
					}
					
					
					
				}
				
				x++;
			}
			
			y++;
		}
		
		return input;
	}

}

