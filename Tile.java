/**
 * 
 * @author Group 1
 * This class represents a tile in a map. The tile has a tileType. 
 */

public class Tile {
	
	private TileType tileType;
	
	
	/**
	 * Constructing a Tile object
	 */
	public Tile() {
		tileType = TileType.EMPTY;
	}
	
	
	
	/**
	 * Checking if tile is of tileType WALL
	 * @return true if the tile is of tileType WALL
	 */
	public boolean isWall() {
		return tileType.equals(TileType.WALL);
	}
	
	
	/**
	 * Checking if tile is of tileType EMPTY
	 * @return true if the tile is of tileType EMPTY
	 */
	public boolean isEmpty() {
		return tileType.equals(TileType.EMPTY);
	}
	
	
	/**
	 * Checking if tile is of tileType PLAYER
	 * @return true if the tile is of tileType PLAYER
	 */
	public boolean isPlayer() {
		return tileType.equals(TileType.PLAYER);
	}
	
	
	/**
	 * Checking if tile is of tileType BOX
	 * @return true if the tile is of tileType BOX
	 */
	public boolean isBox() {
		return tileType.equals(TileType.BOX);
	}
	
	
	
	/**
	 * Checking if tile is of tileType GOAL
	 * @return true if the tile is of tileType GOAL
	 */
	public boolean isGoal() {
		return tileType.equals(TileType.GOAL);
	}
	
	
	
	/**
	 * Checking if tile is of tileType GOALBOX
	 * @return true if the tile is of tileType GOALBOX
	 */
	public boolean isGoalBox() {
		return tileType.equals(TileType.GOALBOX);
	}
	
	
	
	/**
	 * Checking if tile is of tileType GOALPLAYER
	 * @return true if the tile is of tileType GOALPLAYER
	 */
	public boolean isGoalPlayer() {
		return tileType.equals(TileType.GOALPLAYER);
	}
	
	
	
	
	/**
	 * Checking if tile is of tileType PLAYERTWO
	 * @return true if the tile is of tileType PLAYERTWO
	 */
	public boolean isPlayerTwo() {
		return tileType.equals(TileType.PLAYERTWO);
	}
	
	
	
	
	/**
	 * Checking if tile is of tileType GOALPLAYERTWO
	 * @return true if the tile is of tileType GOALPLAYERTWO
	 */
	public boolean isGoalPlayerTwo() {
		return tileType.equals(TileType.GOALPLAYERTWO);
	}
	
	
	
	/**
	 * Setting the tileType
	 * @param tileType
	 */
	public void setTileType(TileType tileType) {
		this.tileType = tileType; 
	}
	
	
	
	/**
	 * Returns the tileType
	 * @return tileType
	 */
	public TileType getTileType() {
		return this.tileType;
	}
	
}
