import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.KeyEvent;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MultiPlayerMap extends Map{

	public MultiPlayerMap(int height, int width) {
		super(height, width);
	}
	
	private int colSecondMan;
	private int rowSecondMan;
	private Direction secondManDirection;
	
	@Override
	public void placeSecondMan(int row, int col) {
		super.placeSecondMan(row, col);
		rowSecondMan = row;
		colSecondMan = col;
	}
	
	public void setSecondManDirection(Direction secondManDirection) {
		this.secondManDirection = secondManDirection;
	}
	
	@Override
	public Map copyMap() {
		
		MultiPlayerMap newMap = new MultiPlayerMap(super.getHeight(), super.getWidth());
		Tile[][] grid = super.getTileGrid();
		newMap.setSecondManDirection(this.secondManDirection);
		newMap.setManDirection(super.getManDirection());
		
		for(int row = 0; row < super.getHeight(); row++) {
			for(int col = 0; col < super.getWidth(); col++) {
				newMap.setTile(row, col, grid[row][col].getTileType());
				if(grid[row][col].getTileType() == TileType.MAN) {
					newMap.placeMan(row, col);
				}
				
				if(grid[row][col].getTileType() == TileType.SECONDMAN) {
					newMap.placeSecondMan(row, col);
				}
			}
		}
		
		return newMap;
	}
	
	public void moveSecondMan(Direction movement) {
		int rowMovement = 0;
		int colMovement = 0;
		Boolean secondManMoved = false;
		this.secondManDirection = movement;
		if (movement == Direction.UP) {
			rowMovement = -1;
		} else if (movement == Direction.DOWN) {
			rowMovement = 1;
		} else if (movement == Direction.LEFT) {
			colMovement = -1;
		} else if (movement == Direction.RIGHT) {
			colMovement = 1;
		}
		
		Tile[][] grid = super.getTileGrid();
		
		if (grid[rowSecondMan+rowMovement][colSecondMan+colMovement].isSolutionorEmpty()) {
			grid[rowSecondMan+rowMovement][colSecondMan+colMovement].setTileType(TileType.SECONDMAN);
			secondManMoved = true;
		} else if (grid[rowSecondMan+rowMovement][colSecondMan+colMovement].isGoal()) {
			grid[rowSecondMan+rowMovement][colSecondMan+colMovement].setTileType(TileType.GOALSECONDMAN);
			secondManMoved = true;
		} else if (grid[rowSecondMan+rowMovement][colSecondMan+colMovement].isBox()) {
			if (grid[rowSecondMan+2*rowMovement][colSecondMan+2*colMovement].isSolutionorEmpty()) {
				grid[rowSecondMan+rowMovement][colSecondMan+colMovement].setTileType(TileType.SECONDMAN);
				grid[rowSecondMan+2*rowMovement][colSecondMan+2*colMovement].setTileType(TileType.BOX);
				secondManMoved = true;
			} else if(grid[rowSecondMan+2*rowMovement][colSecondMan+2*colMovement].isGoal()) {
				grid[rowSecondMan+rowMovement][colSecondMan+colMovement].setTileType(TileType.SECONDMAN);
				grid[rowSecondMan+2*rowMovement][colSecondMan+2*colMovement].setTileType(TileType.GOALBOX);
				secondManMoved = true;
			}
		} else if (grid[rowSecondMan+rowMovement][colSecondMan+colMovement].isGoalBox()) {
			if (grid[rowSecondMan+2*rowMovement][colSecondMan+2*colMovement].isSolutionorEmpty()) {
				grid[rowSecondMan+rowMovement][colSecondMan+colMovement].setTileType(TileType.GOALSECONDMAN);
				grid[rowSecondMan+2*rowMovement][colSecondMan+2*colMovement].setTileType(TileType.BOX);
				secondManMoved = true;
			} else if(grid[rowSecondMan+2*rowMovement][colSecondMan+2*colMovement].isGoal()) {
				grid[rowSecondMan+rowMovement][colSecondMan+colMovement].setTileType(TileType.GOALSECONDMAN);
				grid[rowSecondMan+2*rowMovement][colSecondMan+2*colMovement].setTileType(TileType.GOALBOX);
				secondManMoved = true;
			}
		}
		
		if (secondManMoved.equals(true)) {
			if(grid[rowSecondMan][colSecondMan].isSecondMan()) {
				grid[rowSecondMan][colSecondMan].setTileType(TileType.EMPTY); 
			} else if(grid[rowSecondMan][colSecondMan].isGoalSecondMan()) {
				grid[rowSecondMan][colSecondMan].setTileType(TileType.GOAL);
			}
			colSecondMan = colSecondMan+colMovement;
			rowSecondMan = rowSecondMan+rowMovement;
		}
	}
	
	@Override
	public JPanel displayMap(float gameBoardHeight, float gameBoardWidth) {
		
		JPanel panel = new JPanel(new GridLayout(super.getHeight(), super.getWidth(), 0, 0));
		JLabel label;
		Tile[][] grid = super.getTileGrid();
		
		for (int row = 0; row < super.getHeight(); row ++){
			label = new JLabel();
			for (int col = 0; col < super.getWidth(); col++){
				String url = "";
				if (grid[row][col].isBox()) {
					url = url + "/Users/prasa/Desktop/Ass3/crate.png";
				} else if (grid[row][col].isWall()) {
					url = url + "/Users/prasa/Desktop/Ass3/brickwall.png";
				} else if (grid[row][col].isSolutionorEmpty()) {
					url = url + "/Users/prasa/Desktop/Ass3/tile1.png";
				} else if (grid[row][col].isGoal()) {
					url = url + "/Users/prasa/Desktop/Ass3/goal1.png";
				} else if (grid[row][col].isMan()) {
					if (super.getManDirection() == Direction.DOWN) {
						url = url + "/Users/prasa/Desktop/Ass3/player_down3.png";
					} else if(super.getManDirection() == Direction.UP) {
						url = url + "/Users/prasa/Desktop/Ass3/player_up1.png";
					} else if(super.getManDirection() == Direction.LEFT) {
						url = url + "/Users/prasa/Desktop/Ass3/player_left1.png";
					} else {
						url = url + "/Users/prasa/Desktop/Ass3/player_right1.png";
					}
				} else if (grid[row][col].isGoalMan()) {
					if (super.getManDirection() == Direction.DOWN) {
						url = url + "/Users/prasa/Desktop/Ass3/goal_man_down.png";
					} else if(super.getManDirection() == Direction.UP) {
						url = url + "/Users/prasa/Desktop/Ass3/goal_man_up.png";
					} else if(super.getManDirection() == Direction.LEFT) {
						url = url + "/Users/prasa/Desktop/Ass3/goal_man_left.png";
					} else {
						url = url + "/Users/prasa/Desktop/Ass3/goal_man_right.png";
					}
				} else if (grid[row][col].isGoalBox()) {
					url = url + "/Users/prasa/Desktop/Ass3/goalbox.png";
				} else if (grid[row][col].isSecondMan()) {
					if (secondManDirection == Direction.DOWN) {
						url = url + "/Users/prasa/Desktop/Ass3/player2_down.png";
					} else if(secondManDirection == Direction.UP) {
						url = url + "/Users/prasa/Desktop/Ass3/player2_up.png";
					} else if(secondManDirection == Direction.LEFT) {
						url = url + "/Users/prasa/Desktop/Ass3/player2_left.png";
					} else {
						url = url + "/Users/prasa/Desktop/Ass3/player2_right.png";
					}
				} else if (grid[row][col].isGoalSecondMan()) {
					if (secondManDirection == Direction.DOWN) {
						url = url + "/Users/prasa/Desktop/Ass3/goal_player2_down.png";
					} else if(secondManDirection == Direction.UP) {
						url = url + "/Users/prasa/Desktop/Ass3/goal_player2_up.png";
					} else if(secondManDirection == Direction.LEFT) {
						url = url + "/Users/prasa/Desktop/Ass3/goal_player2_left.png";
					} else {
						url = url + "/Users/prasa/Desktop/Ass3/goal_player2_right.png";
					}
				}
				
				 
				ImageIcon Icon = new ImageIcon(url);
				Image image = Icon.getImage();
				if (gameBoardHeight > gameBoardWidth) {
					float currWidth = gameBoardWidth/(super.getWidth());
					float currHeight = gameBoardWidth/(super.getHeight());
					label.setSize((int)currWidth, (int)currHeight);
				} else {
					float currWidth = gameBoardHeight/(super.getWidth());
					float currHeight = gameBoardHeight/(super.getHeight());
					label.setSize((int)currWidth, (int)currHeight);
				}
				Image newImg = image.getScaledInstance(label.getWidth(), label.getHeight(), Image.SCALE_SMOOTH);
				Icon = new ImageIcon(newImg);
				label = new JLabel(Icon);
				panel.add(label);
				
			}
		}
		
		return panel;
	}
	
	@Override
	public boolean moveMan(int keyCode) {
		Boolean moved = true;
		if (keyCode == KeyEvent.VK_UP) {
			moveMan(Direction.UP);
		} else if (keyCode == KeyEvent.VK_RIGHT) {
			moveMan(Direction.RIGHT);
		} else if (keyCode == KeyEvent.VK_DOWN) {
			moveMan(Direction.DOWN);
		} else if (keyCode == KeyEvent.VK_LEFT) {
			moveMan(Direction.LEFT);
		} else if (keyCode == KeyEvent.VK_W) {
			moveSecondMan(Direction.UP);
		} else if (keyCode == KeyEvent.VK_D) {
			moveSecondMan(Direction.RIGHT);
		} else if (keyCode == KeyEvent.VK_S) {
			moveSecondMan(Direction.DOWN);
		} else if (keyCode == KeyEvent.VK_A) {
			moveSecondMan(Direction.LEFT);
		} else {
			moved = false;
		}
		return moved;
	}
	
}
