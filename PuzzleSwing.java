
import java.awt.Dimension;
import java.awt.GridLayout;
import javax.swing.*;

public class PuzzleSwing extends JFrame {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	
	
	
	public PuzzleSwing(){};
	
	
	
	public void displayMap(Map map){
		JFrame frame = new JFrame("PUZZLE GAME");
		
		JPanel panel = new JPanel(new GridLayout(map.getWidth(), map.getHeight(), 0, 0));
		JLabel label =new JLabel();
		for ( int x = 0; x < map.getWidth(); x ++){
			for ( int y = 0; y< map.getHeight(); y++){
				
				if (map.getTile(x, y).isBox()) {
					label = new JLabel(new ImageIcon("/Users/juliebergeims/desktop/box.png"));
					panel.add(label);
				} else if (map.getTile(x, y).isWall()) {
					panel.add(new JLabel(new ImageIcon("/Users/juliebergeims/desktop/wall.png")));
				} else if (map.getTile(x, y).isEmpty()) {
					panel.add( new JLabel(new ImageIcon("/Users/juliebergeims/desktop/empty.png")));
				} else if (map.getTile(x, y).isGoal()) {
					panel.add(new JLabel(new ImageIcon("/Users/juliebergeims/desktop/trump.png") ));
				} else if (map.getTile(x, y).isMan()) {
					label = new JLabel(new ImageIcon("/Users/juliebergeims/desktop/kim.png"));
					label.setPreferredSize(new Dimension(100,100));
					panel.add(label);
				} else if (map.getTile(x, y).isGoalBox()) {
					panel.add( new JLabel(new ImageIcon("/Users/juliebergeims/desktop/goal.png")));
				} 
				else{}
				// must add goalman as well, have to change Tile class
			}
		}
		
		frame.setContentPane(panel);
        frame.setSize(900, 900);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
	}
	

		
}