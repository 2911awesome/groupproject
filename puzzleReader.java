package groupAssignment;

import java.util.*;
import java.io.*;
public class puzzleReader {
	
	public void readLevelFileForLevel(int level){
	
		String levelDirectory = System.getProperty("user.dir") + java.io.File.separator + "Levels" + java.io.File.separator;
		String filename = levelDirectory + "Level" + level + ".data";
		
		
		
		BufferedReader in;
		try{
			in = new BufferedReader(new FileReader(filename));
		}
		catch (FileNotFoundException e){
		
			System.out.println("Cannot find file \"" + filename + "\".");
			return;
		}

		try{
		
			int numRows = Integer.valueOf(in.readLine().trim()).intValue();
			int numCols = Integer.valueOf(in.readLine().trim()).intValue();
			Map input = new Map(numRows,numCols);
			
			for (int row = 0; row < numRows; row++)
			{
				for (int col = 0; col < numCols; col++)
				{
					readOneSquare(input, (char) in.read(), row, col);
				}

				// Skip over newline at end of row
				in.readLine();
			}
			
		}
		catch (IOException e)
		{
			System.out.println("File improperly formatted, quitting");
			return;
		}
	}
	
	private void readOneSquare( Map input, char ch, int x, int y) {
		

		switch (ch) {
		case '#':
			input.buildWall(x, y);
			break;

		case '.':
			input.placeGoal(x, y);
			break;
		case '@': 
			input.placeMan(x, y);
			break;
		case '$':
			input.placeBox(x, y);
			break;
		case ' ':
			break;
		default :
			System.out.println("problem interpreting character " + ch);
			break;
		
		}


	}

}


