/**
 * 
 * @author Group 1
 * Direction of the movement for a tile of typeType MAN, SECONDMAN or BOX. 
 */
public enum Direction {
	UP, LEFT, DOWN, RIGHT;
}