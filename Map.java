import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.KeyEvent;

import javax.swing.*;

public class Map {

	private Tile[][] grid;
	
	private int height;
	private int width;
	
	private int colMan;
	private int rowMan;
	private Direction manDirection;
	
	public Map(int height, int width) {
		this.height = height;
		this.width = width;
		
		grid = new Tile[height][width];
		int row = 0;
		int col = 0;
		
		for(row = 0; row < height; row++) {
			for(col = 0; col < width; col++) {
				grid[row][col] = new Tile();
			}
		}
	}
	
	public Tile[][] getTileGrid() {
		return this.grid;
	}
	
	public boolean gameComplete(){
		for(int row = 0; row < this.height; row++) {
			for(int col = 0; col < this.width; col++) {
				if (grid[row][col].isBox()){
					return false;
				}
			}
		}
		return true;
	}
	
	public void buildWall(int row, int col) {
		grid[row][col].setTileType(TileType.WALL);
	}
	
	public void placeMan(int row, int col) {
		grid[row][col].setTileType(TileType.MAN);
		rowMan = row;
		colMan = col;
	}
	
	public void placeSecondMan(int row, int col) {
		grid[row][col].setTileType(TileType.SECONDMAN);
	}
	
	public void placeBox(int row, int col) {
		grid[row][col].setTileType(TileType.BOX);
	}
	
	public void placeGoal(int row, int col) {
		grid[row][col].setTileType(TileType.GOAL);
	}
	
	public void setSolution(int row, int col) {
		grid[row][col].setTileType(TileType.SOLUTION);
	}
	
	public void setEmpty(int row, int col) {
		grid[row][col].setTileType(TileType.EMPTY);
	}
	
	public void setGoalBox(int row, int col) {
		grid[row][col].setTileType(TileType.GOALBOX);
	}
	
	public void setGoalSecondMan(int row, int col) {
		grid[row][col].setTileType(TileType.GOALSECONDMAN);
	}
	
	public void setTile(int row, int col, TileType tileType) {
		grid[row][col].setTileType(tileType);
	}
	
	public TileType getTileType(int row, int col) {
		return grid[row][col].getTileType();
	}
	
	public void moveMan(Direction movement) {
		int rowMovement = 0;
		int colMovement = 0;
		Boolean manMoved = false;
		this.manDirection = movement;
		if (movement == Direction.UP) {
			rowMovement = -1;
		} else if (movement == Direction.DOWN) {
			rowMovement = 1;
		} else if (movement == Direction.LEFT) {
			colMovement = -1;
		} else if (movement == Direction.RIGHT) {
			colMovement = 1;
		}
		
		if (grid[rowMan+rowMovement][colMan+colMovement].isSolutionorEmpty()) {
			grid[rowMan+rowMovement][colMan+colMovement].setTileType(TileType.MAN);
			manMoved = true;
		} else if (grid[rowMan+rowMovement][colMan+colMovement].isGoal()) {
			grid[rowMan+rowMovement][colMan+colMovement].setTileType(TileType.GOALMAN);
			manMoved = true;
		} else if (grid[rowMan+rowMovement][colMan+colMovement].isBox()) {
			if (grid[rowMan+2*rowMovement][colMan+2*colMovement].isSolutionorEmpty()) {
				grid[rowMan+rowMovement][colMan+colMovement].setTileType(TileType.MAN);
				grid[rowMan+2*rowMovement][colMan+2*colMovement].setTileType(TileType.BOX);
				manMoved = true;
			} else if(grid[rowMan+2*rowMovement][colMan+2*colMovement].isGoal()) {
				grid[rowMan+rowMovement][colMan+colMovement].setTileType(TileType.MAN);
				grid[rowMan+2*rowMovement][colMan+2*colMovement].setTileType(TileType.GOALBOX);
				manMoved = true;
			}
		} else if (grid[rowMan+rowMovement][colMan+colMovement].isGoalBox()) {
			if (grid[rowMan+2*rowMovement][colMan+2*colMovement].isSolutionorEmpty()) {
				grid[rowMan+rowMovement][colMan+colMovement].setTileType(TileType.GOALMAN);
				grid[rowMan+2*rowMovement][colMan+2*colMovement].setTileType(TileType.BOX);
				manMoved = true;
			} else if(grid[rowMan+2*rowMovement][colMan+2*colMovement].isGoal()) {
				grid[rowMan+rowMovement][colMan+colMovement].setTileType(TileType.GOALMAN);
				grid[rowMan+2*rowMovement][colMan+2*colMovement].setTileType(TileType.GOALBOX);
				manMoved = true;
			}
		}
		
		if (manMoved.equals(true)) {
			if(grid[rowMan][colMan].isMan()) {
				grid[rowMan][colMan].setTileType(TileType.EMPTY); 
			} else if(grid[rowMan][colMan].isGoalMan()) {
				grid[rowMan][colMan].setTileType(TileType.GOAL);
			}
			colMan = colMan+colMovement;
			rowMan = rowMan+rowMovement;
		}
	}
	
	
	public void printMap() {
		int row = 0;
		int col = 0;
		String printSequence = "";
		for(row = 0; row < this.height; row++) {
			printSequence = "";
			for(col = 0; col < this.width; col++) {
				if (this.grid[row][col].isBox()) {
					printSequence = printSequence + "* "; 
				} else if (this.grid[row][col].isWall()) {
					printSequence = printSequence + "x "; 
				} else if (this.grid[row][col].isEmpty()) {
					printSequence = printSequence + "  ";
				} else if (this.grid[row][col].isGoal()) {
					printSequence = printSequence + "! ";
				} else if (this.grid[row][col].isMan()) {
					printSequence = printSequence + "@ ";
				} else if (this.grid[row][col].isGoalBox()) {
					printSequence = printSequence + "$ ";
				} 
			}
			System.out.println(printSequence);
		}
	}
	
	public int getHeight() {
		return this.height;
	}
	
	public int getWidth() {
		return this.width;
	}
	
	public Direction getManDirection() {
		return this.manDirection;
	}
	
	public void setManDirection(Direction manDirection) {
		this.manDirection = manDirection;
	}
	
	public Map copyMap() {
		
		Map newMap = new Map(this.height, this.width);
		newMap.setManDirection(this.manDirection);
		for(int row = 0; row < this.height; row++) {
			for(int col = 0; col < this.width; col++) {
				newMap.setTile(row, col, this.grid[row][col].getTileType());
				
				if(this.grid[row][col].getTileType() == TileType.MAN) {
					newMap.placeMan(row, col);
				}
			}
		}
		
		return newMap;
	}
	
	public JPanel displayMap(float gameBoardHeight, float gameBoardWidth){
		
		JPanel panel = new JPanel(new GridLayout(this.height, this.width, 0, 0));
		JLabel label;
		for (int row = 0; row < this.height; row ++){
			label = new JLabel();
			for (int col = 0; col < this.width; col++){
				String url = "";
				if (this.grid[row][col].isBox()) {
					url = url + "/Users/prasa/Desktop/Ass3/crate.png";
				} else if (this.grid[row][col].isWall()) {
					url = url + "/Users/prasa/Desktop/Ass3/brickwall.png";
				} else if (this.grid[row][col].isSolutionorEmpty()) {
					url = url + "/Users/prasa/Desktop/Ass3/tile1.png";
				} else if (this.grid[row][col].isGoal()) {
					url = url + "/Users/prasa/Desktop/Ass3/goal1.png";
				} else if (this.grid[row][col].isMan()) {
					if (manDirection == Direction.DOWN) {
						url = url + "/Users/prasa/Desktop/Ass3/player_down3.png";
					} else if(manDirection == Direction.UP) {
						url = url + "/Users/prasa/Desktop/Ass3/player_up1.png";
					} else if(manDirection == Direction.LEFT) {
						url = url + "/Users/prasa/Desktop/Ass3/player_left1.png";
					} else {
						url = url + "/Users/prasa/Desktop/Ass3/player_right1.png";
					}
				} else if (this.grid[row][col].isGoalMan()) {
					if (manDirection == Direction.DOWN) {
						url = url + "/Users/prasa/Desktop/Ass3/goal_man_down.png";
					} else if(manDirection == Direction.UP) {
						url = url + "/Users/prasa/Desktop/Ass3/goal_man_up.png";
					} else if(manDirection == Direction.LEFT) {
						url = url + "/Users/prasa/Desktop/Ass3/goal_man_left.png";
					} else {
						url = url + "/Users/prasa/Desktop/Ass3/goal_man_right.png";
					}
				} else if (this.grid[row][col].isGoalBox()) {
					url = url + "/Users/prasa/Desktop/Ass3/goalbox.png";
				}
				
				ImageIcon Icon = new ImageIcon(url);
				Image image = Icon.getImage();
				float currWidth;
				float currHeight;
				if (gameBoardHeight > gameBoardWidth) {
					currWidth = gameBoardWidth/(this.width);
					currHeight = gameBoardWidth/(this.height);
				} else {
					currWidth = gameBoardHeight/(this.width);
					currHeight = gameBoardHeight/(this.height);
				}
				label.setSize((int)currWidth, (int)currHeight);
				Image newImg = image.getScaledInstance(label.getWidth(), label.getHeight(), Image.SCALE_SMOOTH);
				Icon = new ImageIcon(newImg);
				label = new JLabel(Icon);
				panel.add(label);
				
			}
		}
		
		return panel;
	}
	
	public boolean moveMan(int keyCode) {
		Boolean moved = true;
		if (keyCode == KeyEvent.VK_UP) {
			moveMan(Direction.UP);
		} else if (keyCode == KeyEvent.VK_RIGHT) {
			moveMan(Direction.RIGHT);
		} else if (keyCode == KeyEvent.VK_DOWN) {
			moveMan(Direction.DOWN);
		} else if (keyCode == KeyEvent.VK_LEFT) {
			moveMan(Direction.LEFT);
		} else {
			moved = false;
		}
		return moved;
	}

	
}
