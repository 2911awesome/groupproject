# README #

PROJECT GROUP FIRST DISCUSSION 23/04/2017

1.  who are the intended users: are they novice, intermediate or expert, or all sorts of users (do we even understand what this means?)
  
* The intended users are for everyone, but we will start of with the code/program is used for expert, 
* but as time goes on and if the code/program is very successful it will be for novice users

2.what sort of features are basic to the application (i.e. needed by every user)?
  

*inputs that users gives us(arrows keys and enter key), map, ability to move through the map without affecting, 
*move the boxes, the green X(ending areas), the limitations of the map, 
*the direction of the red guy(the direction the red guy is facing also where he can move), generating a puzzle or varied puzzle, 
*the green X's are in different areas, cannot interact with green X's, 

3.what sort of features are needed by different categories of users: how can the interface handle seemingly different requirements?

4.what sort of help or hints (if any) should the system be able to provide to users (and how and when is this help given)?

  *Listing the rules of the game ,also the possible actions the user can do


5.what platforms with what form factors is the system designed to run on?

  *Unix for now
  
  
  
**possible classes in the program : 
**

UserInterface

Map

Character

boxes

stragety(generate solution) 


**
possible assumptions :**

unable to move more than 1 box 

  WHY - otherwise u will be able to move too many things

Character cannot interact with the map 

  WHY - otherwise u will afect the solution to solving the puzzle, ALSO NOTE boxes and green X are objects not part of the map

Inspiration : https://www.sokobanonline.com/play/just-for-fun/85689_a

Possible algo for generating boxes:
Easy: Move box backwards from end goal not too far
Medium: Move box backwards further
Hard: Move box hella far from end goal

1st have an empty map, based on difficulty set the mapsize.
2nd make sure all edges always filled up with walls: makes sure we dont move out
3rd Inside of map should have propability of building a wall: how would you choose where to put the wall
4th Design an algo that sets the walls up
5th Place boxes in 2 locations, can be multiple boxes, then work backwards to a x places back
6th Place man somewhere inside box


You build the solution on some empty map and then on each box that you can build wall, you give a proababily of building a wall
[expanding on the idea before] what this means is that we will make a solution, or a path which the player will move boxes 
to reach the goal. then the walls will be generated around the solution, and we can make different levels based on different 
patterns of walls around the solution.

Rules:
- Thing of edge cases: shit tonne of iffies

UI:
Change wasd to arrows
- 1 person designs the main menu: Make it look preety, Regan does main menu: Start  = start games
                                                                             Instructions = page of rules
                                                                             Difficulty: Easy Hard Medium

- 1 person designs the actual game display: box and man and everything: Hints
                                                                        Main Menu
                                                                        New Map
                                                                        Optional: Change difficulty
                                                                          
- Next Map -> Loading screen

Fail or success

Black hole box