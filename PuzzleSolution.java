package groupAssignment;

import java.util.Random;
import java.util.ArrayList;

/**
 * This code will make the solution, where it will upload 
 * @author Dan
 *
 */
public class puzzleSolution {
	
	public Tile grid[][] ; 
	public int x, y; 
	
	public Map solutionMaker(int diffculty, int width, int height){
		
		Map solution = new Map(width,height);
		int x = 0 ;
		int y = 0 ; 	
		
		switch(diffculty){
		
		case 0: diffculty = 0;
			
		break;
		case 1: diffculty = 1;
			solution.buildWall(1,7);
			solution.buildWall(1, 8);
			solution.buildWall(1, 9);
			solution.buildWall(2, 7);
			solution.placeGoal(2,8);
			solution.buildWall(2, 9);
			solution.buildWall(3, 6);
			solution.buildWall(3, 7);
			solution.setSolution(3, 8);
			solution.buildWall(3, 9);
			solution.buildWall(3, 10);
			solution.buildWall(4, 1);
			solution.buildWall(4, 2);
			solution.buildWall(4, 3);
			solution.buildWall(4, 4);
			solution.buildWall(4, 5);
			solution.buildWall(4, 6);
			solution.placeMan(4, 7);
			solution.placeBox(4, 8);
			solution.buildWall(4, 9);
			solution.buildWall(4, 10);
			solution.buildWall(4, 11);
			solution.buildWall(4, 12);
			solution.buildWall(5, 1);
			solution.placeGoal(5,2);
			solution.setSolution(5, 3);
			solution.setSolution(5, 4);
			solution.setSolution(5, 5);
			
		break;
		case 2: diffculty = 2; 
			solution.setSolution(4,3);
			solution.setSolution(4, 4);
			solution.setSolution(4,5);
			solution.setSolution(4, 6);
			solution.setSolution(4, 7);
			solution.placeGoal(5, 3);
			solution.placeBox(5, 4);
			solution.setSolution(5, 5);
			solution.placeBox(5,6);
			solution.placeGoal(5, 7);
			solution.placeMan(6, 5);
			
		break; 
		case 3: diffculty = 3; 
			solution.setSolution(4,3);
			solution.setSolution(4, 4);
			solution.setSolution(4,5);
			solution.setSolution(4, 6);
			solution.setSolution(4, 7);
			solution.placeGoal(5, 3);
			solution.placeBox(5, 4);
			solution.setSolution(5, 5);
			solution.placeBox(5,6);
			solution.placeGoal(5, 7);
			solution.placeMan(6, 5);
		break; 
		case 4: diffculty = 4; 
		break;
		
		
		case 5: diffculty = 5;
			
			y = height/2;
			solution.placeMan(x+1, y);
			solution.placeBox(x+2, y);
			while(x < width -1  ){
				solution.setSolution( x + 3 ,y );
				x++;
				}
			solution.placeGoal(width-1, y);
		break;

	}
		
		return solution;
	}
	
	
	public Map puzzleEasy(int width, int height){
		    int noGoal = 2;
		    int noBlock = 2;
		    Map puzzle = new Map(width,height);
		    int i = 0;
		    int j = 0;
		   WallBorder(width,height,puzzle);
		    
		    for (i = 0; j < height; i++){
		        for (j = 0; i < width; j++) {
		           
		        	double random = Math.random();
		            if (i == 2 && j == 2) {
		                puzzle.placeMan(i, j); 
		            }
		            else if (random <= .16){
		               puzzle.buildWall(i, j);
		            }
		            else if (random > .16 && random <= .2){
		                if (noGoal != 0) {
		                    puzzle.placeGoal(i, j);
		                    noGoal--;
		                } else {
		                	puzzle.setTile(i, j, TileType.EMPTY);
		                }
		            }
		            else if (random >.2 && random <= .25) {
		                if (noBlock != 0) {
		                    puzzle.placeBox(i, j);
		                    noBlock--;
		                } else {
		                   puzzle.setTile(i, j, TileType.EMPTY);
		                }
		            }
		            else{
		            	puzzle.setTile(i, j, TileType.EMPTY);
		            }
		            
		        }
		        
		    }
		   
		return puzzle;
	}
	
	/**
	 * this puzzle will make a hopefully medium diffculty game.
	 * it will make a player move and push a box randomly aronud the map. 
	 * it will save where the player started at, where the box was first placed and its ending
	 * position of the box will be the goal.
	 * @param rows
	 * @param cols
	 * @return
	 */
	public Map puzzleMedium(int rows, int cols){
		
		Map puzzle = new Map(rows,cols);
		WallBorder(rows,cols,puzzle);
		//WallBorder(rows -1,cols -1,puzzle);
		Random randGen = new Random();
		int ManX = randGen.nextInt(cols-5) + 3;
		int ManY = randGen.nextInt(rows-5) + 3;
		int i = 0;
		int maxMoves = 40;
		int numBoxes = 0;
		ArrayList<Integer> BoxPosition = new ArrayList<Integer>();
		puzzle.placeMan(ManX, ManY);
		while(i < maxMoves){
			int direction = randGen.nextInt(4);
			Direction movement = convertDirection(direction);
			
			//this sees if there is a chance of a box appearing
			if(BoxChance(puzzle,ManX,ManY,direction)){
				i++;
				addBoxPosition(ManX, ManY, BoxPosition, numBoxes);
				numBoxes++;
			
			}else{
				System.out.println(ManX + " "  + ManY);
				puzzle.moveMan(Direction.UP);
			}
				
			i++;
		}
		//scan through the board and get all the positions of the boxes and maake em goals
		//also make all the beginning box positions become boxes
		int x = 0;
		int y = 0;
		while( x < cols){
			while(y < rows){
				if(puzzle.getTileType(x, y) == TileType.BOX){
					puzzle.placeGoal(x, y);
				}
				y++;
			}
			x++;
		}
		while(numBoxes != 0){
			int numBoxX = 0; 
			int numBoxY = 0;
			returnBoxPosition(BoxPosition,numBoxes,numBoxX, numBoxX );
			puzzle.placeBox(numBoxX, numBoxY);
			numBoxes--;
		}
			
		
		return puzzle;
	}
	
	//determines if box is place, if so it will place the box and return a boolean
	public boolean BoxChance(Map puzzle, int ManX, int ManY,int direction){
		Random randGen = new Random();
		int boxChance = randGen.nextInt(100);
		if(boxChance < 60){
			switch(direction){
			case '1': 
				puzzle.placeBox(ManX-1, ManY);
				break;
			case '2': 
				puzzle.placeBox(ManX, ManY+1);
				break;
			case '3': 
				puzzle.placeBox(ManX+1, ManY);
				break;
			case '4':
				puzzle.placeBox(ManX, ManY-1);
				break;
			}
			return true;
		}
		return false;
		
		
		
	}
	public Direction convertDirection(int direction){
		
		Direction output = Direction.UP;
		
		switch(direction){
		case '1': 
			output = Direction.LEFT;
			break;
		case '2': 
			output = Direction.UP;
			break;
		case '3': 
			output = Direction.RIGHT;
			break;
		case '4': 
			output = Direction.DOWN;
			break;
		}
		
		return output;
	}
	//saves the position of the box into the give array list, in the numbox position
	public void addBoxPosition(int ManX, int ManY, ArrayList<Integer> input, int numBox){
		int position  = (ManY*12) + ManX;
		input.add(numBox, position);
	}
	//changes the given X,Y position to the position of the box u want
	public void returnBoxPosition(ArrayList<Integer> input, int numBox, int BoxX, int BoxY){
		int position = 0;
		position = (int) input.get(numBox);
		BoxX = position%12;
		BoxY = (position-BoxX)/12;
	}
	//determines what direction the man will move in, each direction can be variable
	public Map RngManMove(Map puzzle, int direction, int ManX, int ManY){
		switch(direction){
			
		case '1': 
			puzzle.setSolution(ManX, ManY);
			ManX = ManX -1;
			if(puzzle.getTileType(ManX, ManY) == TileType.WALL){
				break;
			}
			puzzle.setSolution(ManX, ManY);
			
			break;
		case '2': 
			puzzle.setSolution(ManX, ManY);
			ManY = ManY + 1;
			if(puzzle.getTileType(ManX, ManY) == TileType.WALL){
				break;
			}
			puzzle.placeMan(ManX, ManY);
			break;
		case '3': 
			puzzle.setSolution(ManX, ManY);
			ManX = ManX +1; 
			if(puzzle.getTileType(ManX, ManY) == TileType.WALL){
				break;
			}
			puzzle.placeMan(ManX, ManY);
			break;
		case '4': 
			puzzle.setSolution(ManX, ManY);
			ManY = ManY -1;
			if(puzzle.getTileType(ManX, ManY) == TileType.WALL){
				break;
			}
			puzzle.placeMan(ManX, ManY);
			break;
			
		}
		return puzzle;
	}
	
	public Map WallBorder(int rows, int cols, Map puzzle){
		int i = 0;
		int j = 0;
		while(i == 0 && 0 < j && j < rows ){
    		puzzle.buildWall(i, j);
    		j++;
    	}
	    while(j == rows && 0 < i && i < cols ){
    		puzzle.buildWall(i, j);
    		i++;
    	}
	    while(j == 0 && 0 < i && i < cols ){
    		puzzle.buildWall(i, j);
    		i++;
    	}
	    while(i == cols && 0 < j && j < rows ){
    		puzzle.buildWall(i, j);
    		j++;
    	}
	    return puzzle;
	}
	/**
	 * public Map finalSolution(int width, int height, int diffculty){
		
		Map solution = new Map(width,height);
		
		Random randGen = new Random();
		
		int goalY = randGen.nextInt(height);
		int goalX = randGen.nextInt(width);
		int boxY = randGen.nextInt(width);
		int boxX = randGen.nextInt(height);
		
		while (boxX == goalX){
			boxX = randGen.nextInt(width);
			goalX = randGen.nextInt(width);
		}
		while(boxY == goalY){
			boxY = randGen.nextInt(height);
			goalY = randGen.nextInt(height);
		}
		
		solution.setSolution(goalY, goalX);
		solution.placeBox(boxX, boxY);
		
		while(boxX != goalX){
			solution.setSolution(boxX, boxY);
			boxX++;
		}
		
		solution.setSolution(boxX, boxY+1);
		solution.setSolution(boxX-1, boxY+1);
		while(boxY != goalY){
			solution.setSolution(boxX, boxY);
			boxY--;
		}
		
		return solution;
	}
	 */
	
/**	public Map RNGSolution(int width, int height, Map input, int diffculty){
		
		Map solution = input;
		
		y = height/2;
		solution.placeMan(x+1, y);
		solution.placeBox(x+2, y);
		while(x < width -1  ){
			solution.setSolution( x + 3 ,y );
			x++;
			}
		solution.placeGoal(width-1, y);
		
		
		 * First place a goal in a random position on the board
		 * then place a box in a random distance away from the goal
		 * then find the straight line distance from each other and possible one turn
		 * as it will be in the same Y value
		 * add turns based on the diffculty
		 
		
		
		return solution;
	}*/
}


